package com.example.temp;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Service
public class RestService {

    private final RestTemplate restTemplate;

    public RestService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public String getPostsPlainJSON() {
        String url = "https://api.openweathermap.org/data/2.5/onecall?lat=59.9386&lon=30.3141&units=metric&exclude=hourly,minutely&appid=8af55661ff6d769e8951bfad151f38e9";
        String jsonString = this.restTemplate.getForObject(url, String.class);
        try {
            JSONObject weatherJsonObject = (JSONObject) JSONValue.parseWithException(jsonString);
            JSONArray pressureArray = (JSONArray) weatherJsonObject.get("daily");
            int[] pressureFiveDays = new int[5];
            double differenceTemp[] = new double[5];
            for(int i = 0; i < 5; i++) {
                    JSONObject weatherData = (JSONObject) pressureArray.get(i);
                    pressureFiveDays[i] = Integer.valueOf(weatherData.get("pressure").toString());
                    JSONObject tempJsonObject = (JSONObject) JSONValue.parseWithException(weatherData.get("temp").toString());
                    double differenceMornNight = Math.abs(Double.valueOf(tempJsonObject.get("morn").toString()) - Double.valueOf(tempJsonObject.get("night").toString()));
                    differenceTemp[i] = differenceMornNight;
                }
            int maxPressure = Arrays.stream(pressureFiveDays).max().getAsInt();
            double minDifferenceTemp = Arrays.stream(differenceTemp).min().getAsDouble();
            System.out.println("Max pressure: " + maxPressure);
            System.out.println("Min temp difference: " + minDifferenceTemp);
        } catch (org.json.simple.parser.ParseException e) {
                e.printStackTrace();
            }
        return this.restTemplate.getForObject(url, String.class);
    }
}